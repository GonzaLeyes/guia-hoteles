$(function() {
 		 $("[data-toggle='tooltip']").tooltip();
 		 $("[data-toggle='popover']").popover();
 		 $('.carousel').carousel({
 		 	interval: 2000
 		 });
 		 $('#reservar').on('show.bs.modal', function(e){
 		 	$('#contactoBtn').removeClass('btn-success');
 		 	$('#contactoBtn').addClass('btn-dark');
 		 	$('#contactoBtn').prop('disabled', true);
 		 });
 		 $('#reservar').on('hide.bs.modal', function(e){
 		 	$('#contactoBtn').removeClass('btn-dark');
 		 	$('#contactoBtn').addClass('btn-success');
 		 	$('#contactoBtn').prop('disabled', false);
 		 });

		});